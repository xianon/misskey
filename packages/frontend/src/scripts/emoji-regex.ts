import twemojiRegex from 'twemoji-parser/dist/lib/regex.js';

export const emojiRegex = new RegExp(`(${twemojiRegex.source})`);
