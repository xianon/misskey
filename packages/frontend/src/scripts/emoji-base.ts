const twemojiSvgBase = '/twemoji';

export function char2twemojiFilePath(char: string): string {
	let codes = Array.from(char).map(x => x.codePointAt(0)?.toString(16));
	if (!codes.includes('200d')) codes = codes.filter(x => x !== 'fe0f');
	codes = codes.filter(x => x && x.length);
	const fileName = codes.join('-');
	return `${twemojiSvgBase}/${fileName}.svg`;
}
