import * as os from '@/os';
import { $i } from '@/account';

export const ACHIEVEMENT_TYPES = [
	'notes1',
	'notes10',
	'notes100',
	'notes500',
	'notes1000',
	'notes5000',
	'notes10000',
	'notes20000',
	'notes30000',
	'notes40000',
	'notes50000',
	'notes60000',
	'notes70000',
	'notes80000',
	'notes90000',
	'notes100000',
	'login3',
	'login7',
	'login15',
	'login30',
	'login60',
	'login100',
	'login200',
	'login300',
	'login400',
	'login500',
	'login600',
	'login700',
	'login800',
	'login900',
	'login1000',
	'passedSinceAccountCreated1',
	'passedSinceAccountCreated2',
	'passedSinceAccountCreated3',
	'loggedInOnBirthday',
	'loggedInOnNewYearsDay',
	'noteClipped1',
	'noteFavorited1',
	'myNoteFavorited1',
	'profileFilled',
	'markedAsCat',
	'following1',
	'following10',
	'following50',
	'following100',
	'following300',
	'followers1',
	'followers10',
	'followers50',
	'followers100',
	'followers300',
	'followers500',
	'followers1000',
	'collectAchievements30',
	'viewAchievements3min',
	'iLoveMisskey',
	'foundTreasure',
	'client30min',
	'noteDeletedWithin1min',
	'postedAtLateNight',
	'postedAt0min0sec',
	'selfQuote',
	'htl20npm',
	'viewInstanceChart',
	'outputHelloWorldOnScratchpad',
	'open3windows',
	'driveFolderCircularReference',
	'reactWithoutRead',
	'clickedClickHere',
	'justPlainLucky',
	'setNameToSyuilo',
	'cookieClicked',
	'brainDiver',
] as const;

export const ACHIEVEMENT_BADGES = {
	'notes1': {
		img: '/twemoji/1f4dd.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'notes10': {
		img: '/twemoji/1f4d1.svg',
		bg: null,
		frame: 'bronze',
	},
	'notes100': {
		img: '/twemoji/1f4d2.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'notes500': {
		img: '/twemoji/1f4da.svg',
		bg: null,
		frame: 'bronze',
	},
	'notes1000': {
		img: '/twemoji/1f5c3.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'notes5000': {
		img: '/twemoji/1f304.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'notes10000': {
		img: '/twemoji/1f3d9.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'silver',
	},
	'notes20000': {
		img: '/twemoji/1f307.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'silver',
	},
	'notes30000': {
		img: '/twemoji/1f306.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'silver',
	},
	'notes40000': {
		img: '/twemoji/1f303.svg',
		bg: 'linear-gradient(0deg, rgb(197 69 192), rgb(2 112 155))',
		frame: 'silver',
	},
	'notes50000': {
		img: '/twemoji/1fa90.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'gold',
	},
	'notes60000': {
		img: '/twemoji/2604.svg',
		bg: 'linear-gradient(0deg, rgb(197 69 192), rgb(2 112 155))',
		frame: 'gold',
	},
	'notes70000': {
		img: '/twemoji/1f30c.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'gold',
	},
	'notes80000': {
		img: '/twemoji/1f30c.svg',
		bg: 'linear-gradient(0deg, rgb(197 69 192), rgb(2 112 155))',
		frame: 'gold',
	},
	'notes90000': {
		img: '/twemoji/1f30c.svg',
		bg: 'linear-gradient(0deg, rgb(255 232 119), rgb(255 140 41))',
		frame: 'gold',
	},
	'notes100000': {
		img: '/twemoji/267e.svg',
		bg: 'linear-gradient(0deg, rgb(255 232 119), rgb(255 140 41))',
		frame: 'platinum',
	},
	'login3': {
		img: '/twemoji/1f331.svg',
		bg: null,
		frame: 'bronze',
	},
	'login7': {
		img: '/twemoji/1f331.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'login15': {
		img: '/twemoji/1f331.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'bronze',
	},
	'login30': {
		img: '/twemoji/1fab4.svg',
		bg: null,
		frame: 'bronze',
	},
	'login60': {
		img: '/twemoji/1fab4.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'login100': {
		img: '/twemoji/1fab4.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'silver',
	},
	'login200': {
		img: '/twemoji/1f333.svg',
		bg: null,
		frame: 'silver',
	},
	'login300': {
		img: '/twemoji/1f333.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'silver',
	},
	'login400': {
		img: '/twemoji/1f333.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'silver',
	},
	'login500': {
		img: '/twemoji/1f304.svg',
		bg: null,
		frame: 'silver',
	},
	'login600': {
		img: '/twemoji/1f304.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'gold',
	},
	'login700': {
		img: '/twemoji/1f304.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'gold',
	},
	'login800': {
		img: '/twemoji/1f307.svg',
		bg: null,
		frame: 'gold',
	},
	'login900': {
		img: '/twemoji/1f307.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'gold',
	},
	'login1000': {
		img: '/twemoji/1f307.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'platinum',
	},
	'noteClipped1': {
		img: '/twemoji/1f587.svg',
		bg: null,
		frame: 'bronze',
	},
	'noteFavorited1': {
		img: '/twemoji/1f31f.svg',
		bg: null,
		frame: 'bronze',
	},
	'myNoteFavorited1': {
		img: '/twemoji/1f320.svg',
		bg: null,
		frame: 'silver',
	},
	'profileFilled': {
		img: '/twemoji/1f44c.svg',
		bg: 'linear-gradient(0deg, rgb(187 183 59), rgb(255 143 77))',
		frame: 'bronze',
	},
	'markedAsCat': {
		img: '/twemoji/1f408.svg',
		bg: 'linear-gradient(0deg, rgb(187 183 59), rgb(255 143 77))',
		frame: 'bronze',
	},
	'following1': {
		img: '/twemoji/2618.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'following10': {
		img: '/twemoji/1f6b8.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'following50': {
		img: '/twemoji/1f91d.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'following100': {
		img: '/twemoji/1f4af.svg',
		bg: 'linear-gradient(0deg, rgb(255 53 184), rgb(255 206 69))',
		frame: 'silver',
	},
	'following300': {
		img: '/twemoji/1f970.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'silver',
	},
	'followers1': {
		img: '/twemoji/2618.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'followers10': {
		img: '/twemoji/1f44b.svg',
		bg: 'linear-gradient(0deg, rgb(59 187 116), rgb(199 211 102))',
		frame: 'bronze',
	},
	'followers50': {
		img: '/twemoji/1f411.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'bronze',
	},
	'followers100': {
		img: '/twemoji/1f60e.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'silver',
	},
	'followers300': {
		img: '/twemoji/1f3c6.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'silver',
	},
	'followers500': {
		img: '/twemoji/1f4e1.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'gold',
	},
	'followers1000': {
		img: '/twemoji/1f451.svg',
		bg: 'linear-gradient(0deg, rgb(255 232 119), rgb(255 140 41))',
		frame: 'platinum',
	},
	'collectAchievements30': {
		img: '/twemoji/1f3c5.svg',
		bg: 'linear-gradient(0deg, rgb(255 77 77), rgb(247 155 214))',
		frame: 'silver',
	},
	'viewAchievements3min': {
		img: '/twemoji/1f3c5.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'bronze',
	},
	'iLoveMisskey': {
		img: '/twemoji/2764.svg',
		bg: 'linear-gradient(0deg, rgb(255 77 77), rgb(247 155 214))',
		frame: 'silver',
	},
	'foundTreasure': {
		img: '/twemoji/1f3c6.svg',
		bg: 'linear-gradient(0deg, rgb(197 69 192), rgb(2 112 155))',
		frame: 'gold',
	},
	'client30min': {
		img: '/twemoji/1f552.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'bronze',
	},
	'noteDeletedWithin1min': {
		img: '/twemoji/1f5d1.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'bronze',
	},
	'postedAtLateNight': {
		img: '/twemoji/1f319.svg',
		bg: 'linear-gradient(0deg, rgb(197 69 192), rgb(2 112 155))',
		frame: 'bronze',
	},
	'postedAt0min0sec': {
		img: '/twemoji/1f55b.svg',
		bg: 'linear-gradient(0deg, rgb(58 231 198), rgb(37 194 255))',
		frame: 'bronze',
	},
	'selfQuote': {
		img: '/twemoji/1f4dd.svg',
		bg: null,
		frame: 'bronze',
	},
	'htl20npm': {
		img: '/twemoji/1f30a.svg',
		bg: 'linear-gradient(0deg, rgb(220 223 225), rgb(172 192 207))',
		frame: 'bronze',
	},
	'viewInstanceChart': {
		img: '/twemoji/1f4ca.svg',
		bg: 'linear-gradient(0deg, rgb(58 231 198), rgb(37 194 255))',
		frame: 'bronze',
	},
	'outputHelloWorldOnScratchpad': {
		img: '/twemoji/1f530.svg',
		bg: 'linear-gradient(0deg, rgb(58 231 198), rgb(37 194 255))',
		frame: 'bronze',
	},
	'open3windows': {
		img: '/twemoji/1f5a5.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'bronze',
	},
	'driveFolderCircularReference': {
		img: '/twemoji/1f4c2.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'bronze',
	},
	'reactWithoutRead': {
		img: '/twemoji/2753.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'bronze',
	},
	'clickedClickHere': {
		img: '/twemoji/2757.svg',
		bg: 'linear-gradient(0deg, rgb(144 224 255), rgb(255 168 252))',
		frame: 'bronze',
	},
	'justPlainLucky': {
		img: '/twemoji/1f340.svg',
		bg: 'linear-gradient(0deg, rgb(187 183 59), rgb(255 143 77))',
		frame: 'silver',
	},
	'setNameToSyuilo': {
		img: '/twemoji/1f36e.svg',
		bg: 'linear-gradient(0deg, rgb(187 183 59), rgb(255 143 77))',
		frame: 'bronze',
	},
	'passedSinceAccountCreated1': {
		img: '/twemoji/31-20e3.svg',
		bg: null,
		frame: 'bronze',
	},
	'passedSinceAccountCreated2': {
		img: '/twemoji/32-20e3.svg',
		bg: null,
		frame: 'silver',
	},
	'passedSinceAccountCreated3': {
		img: '/twemoji/33-20e3.svg',
		bg: null,
		frame: 'gold',
	},
	'loggedInOnBirthday': {
		img: '/twemoji/1f382.svg',
		bg: 'linear-gradient(0deg, rgb(255 77 77), rgb(247 155 214))',
		frame: 'silver',
	},
	'loggedInOnNewYearsDay': {
		img: '/twemoji/1f38d.svg',
		bg: 'linear-gradient(0deg, rgb(255 144 144), rgb(255 232 168))',
		frame: 'silver',
	},
	'cookieClicked': {
		img: '/twemoji/1f36a.svg',
		bg: 'linear-gradient(0deg, rgb(187 183 59), rgb(255 143 77))',
		frame: 'bronze',
	},
	'brainDiver': {
		img: '/twemoji/1f9e0.svg',
		bg: 'linear-gradient(0deg, rgb(144, 224, 255), rgb(255, 168, 252))',
		frame: 'bronze',
	},
} as const satisfies Record<typeof ACHIEVEMENT_TYPES[number], {
	img: string;
	bg: string | null;
	frame: 'bronze' | 'silver' | 'gold' | 'platinum';
}>;

export const claimedAchievements: typeof ACHIEVEMENT_TYPES[number][] = ($i && $i.achievements) ? $i.achievements.map(x => x.name) : [];

const claimingQueue = new Set<string>();

export async function claimAchievement(type: typeof ACHIEVEMENT_TYPES[number]) {
	if ($i == null) return;
	if (claimedAchievements.includes(type)) return;
	claimingQueue.add(type);
	claimedAchievements.push(type);
	await new Promise(resolve => setTimeout(resolve, (claimingQueue.size - 1) * 500));
	window.setTimeout(() => {
		claimingQueue.delete(type);
	}, 500);
	os.api('i/claim-achievement', { name: type });
}

if (_DEV_) {
	(window as any).unlockAllAchievements = () => {
		for (const t of ACHIEVEMENT_TYPES) {
			claimAchievement(t);
		}
	};
}
